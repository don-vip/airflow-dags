"""
### Generates canary events for testing the event pipeline
"""
import json
import logging
from datetime import datetime, timedelta
from typing import Any, Dict, List

import fsspec
from airflow.decorators import task

from analytics.config.dag_config import alerts_email, artifact, create_easy_dag
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator

dag_id = "canary_events_hourly"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2024, 3, 21, 22),
    sla=timedelta(hours=6),
    email=alerts_email,
    stream_config_url="https://meta.wikimedia.org/w/api.php?action=streamconfigs&constraints=canary_events_enabled=1",
    refinery_job_jar=artifact("refinery-job-0.2.33-shaded.jar"),
    event_intake_service_urls_file="file:///etc/refinery/event_intake_service_urls.yaml",
    catchup=False,
    schema_base_uris=[
        "https://schema.discovery.wmnet/repositories/primary/jsonschema",
        "https://schema.discovery.wmnet/repositories/secondary/jsonschema",
    ],
    event_stream_config_uri="https://meta.wikimedia.org/w/api.php",
    event_stream_config_uri_parameter="file://$(pwd)/event_intake_service_urls.json",
)

logger = logging.getLogger(__name__)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="10 * * * *",  # 10 minutes after the hour
    tags=["hourly", "from_hive", "to_iceberg"],
    default_args={"sla": props.sla},
    catchup=props.catchup,
    # Bump max_active_tasks to schedule more small jobs
    # see comment in the produce_canary_events task
    max_active_tasks=10,
) as dag:

    @task(do_xcom_push=True)
    def fetch_streams_configurations() -> List[Dict[str, str | Any]]:
        url = props.stream_config_url
        logger.info(f"Fetching streams from {url}")
        with fsspec.open(url) as f:
            configurations = json.load(f)["streams"].values()
        logger.info(
            f"{len(configurations)} stream(s) to produce canary events: {[e['stream'] for e in configurations]}"
        )
        return list(configurations)

    @task(
        do_xcom_push=True,
        templates_dict={"dt": "{{ data_interval_start.in_timezone('UTC').to_iso8601_string() }}"},
    )
    def prepare_scripts_commands(configs, **kwargs):
        logger.info(f"received configs: {configs}")
        return [
            "spark3-submit --driver-cores 1 --master local "
            "--class org.wikimedia.analytics.refinery.job.ProduceCanaryEvents "
            f"{props.refinery_job_jar} "
            f"--schema_base_uris={','.join(props.schema_base_uris)} "
            f"--event_stream_config_uri={props.event_stream_config_uri} "
            f"--event_service_config_uri={props.event_stream_config_uri_parameter} "
            f"--stream_name {conf['stream']} "
            f"--timestamp {kwargs['templates_dict']['dt']} "
            "--dry_run false"
            for conf in configs
            if "destination_event_service" in conf
        ]

    produce_canary_events = SimpleSkeinOperator.partial(
        task_id="produce_canary_events",
        files={"event_intake_service_urls.json": props.event_intake_service_urls_file},
        sla=None,  # SLA are not supported for mapped tasks
        # Override parallelization for this DAG to run more small jobs at once
        # Note: the jobs use the `launchers` queue to not overload the `production`one
        max_active_tis_per_dag=10,
        queue="launchers",
    )

    produce_canary_events.expand(script=prepare_scripts_commands(fetch_streams_configurations()))
