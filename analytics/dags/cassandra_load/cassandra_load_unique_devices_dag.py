"""
Loads Hive unique devices into Cassandra

The loading is done with 2 dags: daily and monthly.

"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Data sources and destination.
    hive_unique_devices_per_domain_daily_table="wmf.unique_devices_per_domain_daily",
    hive_unique_devices_per_domain_monthly_table="wmf.unique_devices_per_domain_monthly",
    hive_unique_devices_per_project_family_daily_table="wmf.unique_devices_per_project_family_daily",
    hive_unique_devices_per_project_family_monthly_table="wmf.unique_devices_per_project_family_monthly",
    cassandra_unique_devices_table="aqs.local_group_default_T_unique_devices.data",
    # DAG start dates.
    daily_dag_start_date=datetime(2023, 7, 18),
    monthly_dag_start_date=datetime(2023, 7, 1),
    # HQL query paths
    daily_dag_hql=f"{hql_directory}/cassandra/daily/load_cassandra_unique_devices_daily.hql",
    monthly_dag_hql=f"{hql_directory}/cassandra/monthly/load_cassandra_unique_devices_monthly.hql",
    # SLAs and alerts email.
    daily_dag_sla=timedelta(hours=10),
    monthly_dag_sla=timedelta(days=5),
    alerts_email=alerts_email,
)


default_tags = ["from_hive", "to_cassandra", "uses_hql"]

daily_tags = [
    "daily",
    "requires_wmf_unique_devices_per_domain_daily",
    "requires_wmf_unique_devices_per_project_family_daily",
]

monthly_tags = [
    "monthly",
    "requires_wmf_unique_devices_per_domain_monthly",
    "requires_wmf_unique_devices_per_project_family_monthly",
]

default_query_parameters = {
    "destination_table": props.cassandra_unique_devices_table,
    # Setting coalesce_partitions to 6 is important
    # as it defines how many parallel loaders we use for cassandra.
    # We currently use 6 as there are 6 cassandra hosts
    "coalesce_partitions": 6,
}


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_unique_devices_daily",
    doc_md="Loads Hive unique devices data in cassandra daily",
    start_date=props.daily_dag_start_date,
    schedule="@daily",
    tags=default_tags + daily_tags,
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    sensors = [
        dataset("hive_wmf_unique_devices_per_domain_daily").get_sensor_for(daily_dag),
        dataset("hive_wmf_unique_devices_per_project_family_daily").get_sensor_for(daily_dag),
    ]

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.daily_dag_hql,
        query_parameters={
            **default_query_parameters,
            "source_table_per_domain": props.hive_unique_devices_per_domain_daily_table,
            "source_table_per_project_family": props.hive_unique_devices_per_project_family_daily_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
        },
        # The default spark resources configuration is enough for this job
        # as it processes relatively small data
    )

    sensors >> etl


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_unique_devices_monthly",
    doc_md="Loads Hive unique devices data in cassandra monthly",
    start_date=props.monthly_dag_start_date,
    schedule="@monthly",
    tags=default_tags + monthly_tags,
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    sensors = [
        dataset("hive_wmf_unique_devices_per_domain_monthly").get_sensor_for(monthly_dag),
        dataset("hive_wmf_unique_devices_per_project_family_monthly").get_sensor_for(monthly_dag),
    ]

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.monthly_dag_hql,
        query_parameters={
            **default_query_parameters,
            "source_table_per_domain": props.hive_unique_devices_per_domain_monthly_table,
            "source_table_per_project_family": props.hive_unique_devices_per_project_family_monthly_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
        },
        # The default spark resources configuration is enough for this job
        # as it processes relatively small data
    )

    sensors >> etl
