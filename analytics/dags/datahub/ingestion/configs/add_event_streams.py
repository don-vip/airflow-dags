"""Transformer for kafka recipe that adds schemas and associated event streams

TODO: move out to artifact repository when that is available.

This transform does a few things:
    - Adds an Event Streams platform to DataHub.
    - Adds a browser path to kafka topics for easier organization.
    - Adds schemas to kafka topics that have them.
    - Adds top-level description to kafka topics extracted from the schema.
    - Adds event stream and associates it to kafka topics via lineage.
"""

import sys
from datetime import datetime
from typing import Dict, List, Optional

import datahub.emitter.mce_builder as builder
import requests
import yaml
from datahub.ingestion.api.common import PipelineContext
from datahub.ingestion.extractor.json_schema_util import JsonSchemaTranslator
from datahub.ingestion.transformer.add_dataset_browse_path import (
    AddDatasetBrowsePathConfig,
    AddDatasetBrowsePathTransformer,
)
from datahub.ingestion.transformer.base_transformer import (
    BaseTransformer,
    LegacyMCETransformer,
)
from datahub.metadata.com.linkedin.pegasus2avro.dataplatform import DataPlatformInfo
from datahub.metadata.com.linkedin.pegasus2avro.metadata.snapshot import (
    DataPlatformSnapshot,
)
from datahub.metadata.schema_classes import (
    AuditStampClass,
    BrowsePathsClass,
    DatasetLineageTypeClass,
    DatasetPropertiesClass,
    DatasetSnapshotClass,
    MetadataChangeEventClass,
    OtherSchemaClass,
    PlatformTypeClass,
    SchemaFieldClass,
    SchemaMetadataClass,
    UpstreamClass,
    UpstreamLineageClass,
)
from pydantic import Extra

is_dry_run = True if "--dry-run" in sys.argv or "-n" in sys.argv else False


class AddEventStreamMetadataConfig(AddDatasetBrowsePathConfig):
    use_proxy: bool = False
    schema_repos: List[str] = [
        "https://schema.wikimedia.org/repositories/primary/jsonschema",
        "https://schema.wikimedia.org/repositories/secondary/jsonschema",
    ]
    env: str = "PROD"
    path_templates: List[str] = ["/ENV/PLATFORM/DATASET_PARTS"]


def create_event_streams_platform(ctx: PipelineContext):
    event_streams_info = DataPlatformInfo(
        datasetNameDelimiter=".",
        name="eventstreams",
        displayName="Event Streams",
        type=PlatformTypeClass.OTHERS,
        logoUrl="https://upload.wikimedia.org/wikipedia/labs/6/62/EventPlatformIcon.png",
    )
    event_streams_platform = DataPlatformSnapshot("urn:li:dataPlatform:eventstreams", [event_streams_info])
    if not is_dry_run:
        ctx.graph.emit_mce(MetadataChangeEventClass(proposedSnapshot=event_streams_platform))


def get_topic_to_config_map(stream_config):
    _topic_to_schema = {}
    for stream, data in stream_config["streams"].items():
        for topic in data["topics"]:
            _topic_to_schema[topic] = data
    return _topic_to_schema


def create_schema(
    schema_name: str, platform_urn: str, dataset_urn: Optional[str], schema_fields: List[SchemaFieldClass]
) -> SchemaMetadataClass:
    return SchemaMetadataClass(
        schemaName=schema_name,  # not used
        platform=platform_urn,  # platform must be an urn
        version=0,  # this only takes an int. so we can't use semantic versioning. Set as 0 for no version info.
        created=AuditStampClass(time=int(round(datetime.now().timestamp())), actor="urn:li:corpuser:ingestion"),
        lastModified=AuditStampClass(time=int(round(datetime.now().timestamp())), actor="urn:li:corpuser:ingestion"),
        dataset=dataset_urn,
        hash="",
        platformSchema=OtherSchemaClass(rawSchema=""),
        fields=schema_fields,
    )


def create_slash_seperated_data_path(
    ctx: PipelineContext, urn: str, config: Dict, paths: Optional[BrowsePathsClass] = None
) -> BrowsePathsClass:
    """Helper function that replaces '.' with '/' before creating a BrowsePathClass.

    Also places eventlogging_ topics in its own browser path since it's not '.' or '/' seperated.

    :param ctx: PipelineContext
    :param urn: urn to extract env, platform, and path
    :param config: Superset of AddDatasetBrowsePathConfig
    :param paths: Path gets appended to this BrowsePathsClass if provided
    :return: BrowsePathsClass with the path being a slash-seperated dataset part
    """
    # Ignore superset config values
    AddDatasetBrowsePathConfig.Config.extra = Extra.allow
    transformer_config = AddDatasetBrowsePathConfig(**config).dict()

    # Put any legacy eventlogging_ datasets into its own folder since it's neither '.' or '/' seperated.
    if builder.dataset_urn_to_key(urn).name.startswith("eventlogging_"):

        def replace_dataset_parts(path: str) -> str:
            return path.replace("DATASET_PARTS", "eventlogging/DATASET_PARTS")

        transformer_config["path_templates"] = list(
            map(
                replace_dataset_parts,
                transformer_config["path_templates"],
            )
        )

    transformer = AddDatasetBrowsePathTransformer.create(transformer_config, ctx)
    return transformer.transform_aspect(urn, BrowsePathsClass.get_aspect_name(), paths)


def create_description(description: str) -> DatasetPropertiesClass:
    return DatasetPropertiesClass(description=description)


def get_latest_schema_dict(schema_repo_url: str, schema_title: str) -> Optional[Dict]:
    full_url = f"{schema_repo_url}/{schema_title}/latest"
    try:
        resp = requests.get(full_url)
        resp.raise_for_status()
        return yaml.safe_load(resp.text)  # type: ignore
    except (requests.exceptions.RequestException, yaml.YAMLError):
        return None


def get_schema_fields_from_dict(schema_dict: Dict) -> List[SchemaFieldClass]:
    return list(JsonSchemaTranslator.get_fields_from_schema(schema_dict))


def create_event_stream_mce(stream, env, aspects: Optional[List[builder.Aspect]] = None) -> MetadataChangeEventClass:
    if aspects is None:
        aspects = []

    urn = builder.make_dataset_urn_with_platform_instance("eventstreams", stream, None, env)
    snapshot = DatasetSnapshotClass(urn, aspects)
    return MetadataChangeEventClass(proposedSnapshot=snapshot)


def add_event_schema_metadata(
    dataset_urn: str,
    mce: MetadataChangeEventClass,
    event_stream_mce: MetadataChangeEventClass,
    schema_dict: dict,
    stream_config: dict,
):
    keys = builder.dataset_urn_to_key(dataset_urn)

    # Set schema description as the description of the dataset.
    # This does not overwrite user-edited fields (see EditableDatasetPropertiesClass).
    if "description" in schema_dict:
        description = create_description(schema_dict["description"])
        builder.set_aspect(mce, description, DatasetPropertiesClass)
        builder.set_aspect(event_stream_mce, description, DatasetPropertiesClass)

    # Add event stream as upstream lineage to kafka topic
    lineage = UpstreamClass(event_stream_mce.proposedSnapshot.urn, DatasetLineageTypeClass.COPY)
    builder.set_aspect(mce, UpstreamLineageClass([lineage]), UpstreamLineageClass)

    # Add schema fields
    fields = list(JsonSchemaTranslator.get_fields_from_schema(schema_dict))

    topic_schema = create_schema(
        stream_config["schema_title"], builder.make_data_platform_urn(keys.platform), dataset_urn, fields
    )
    builder.set_aspect(mce, topic_schema, SchemaMetadataClass)
    event_schema = create_schema(
        stream_config["schema_title"],
        builder.make_data_platform_urn("eventstreams"),
        event_stream_mce.proposedSnapshot.urn,
        fields,
    )
    builder.set_aspect(event_stream_mce, event_schema, SchemaMetadataClass)


class AddEventStreamMetadata(BaseTransformer, LegacyMCETransformer):
    ctx: PipelineContext

    config: AddEventStreamMetadataConfig

    def entity_types(self) -> List[str]:
        return ["dataset"]

    def __init__(self, config: AddEventStreamMetadataConfig, ctx: PipelineContext):
        super().__init__()
        self.ctx = ctx
        self.config = config

        try:
            if self.config.use_proxy:
                url = "https://api-ro.discovery.wmnet/w/api.php"
                headers = {"Host": "meta.wikimedia.org"}
                payload = {"action": "streamconfigs", "format": "json"}

                resp = requests.get(url, headers=headers, params=payload).json()
            else:
                url = "https://meta.wikimedia.org/w/api.php?action=streamconfigs&format=json"
                resp = requests.get(url).json()

            self.topic_to_config = get_topic_to_config_map(resp)
            create_event_streams_platform(ctx)
        except requests.exceptions.ConnectionError:
            print("Unable to fetch stream configs. No-oping transformer.")
            self.topic_to_config = None

    @classmethod
    def create(cls, config_dict: dict, ctx: PipelineContext) -> "AddEventStreamMetadata":
        config = AddEventStreamMetadataConfig.parse_obj(config_dict)
        return cls(config, ctx)

    def transform_one(self, mce: MetadataChangeEventClass) -> MetadataChangeEventClass:
        if not isinstance(mce.proposedSnapshot, DatasetSnapshotClass) or not self.topic_to_config:
            return mce

        snapshot: DatasetSnapshotClass = mce.proposedSnapshot
        dataset_urn = snapshot.urn
        topic = builder.dataset_urn_to_key(dataset_urn).name

        has_event_stream = topic in self.topic_to_config

        path = create_slash_seperated_data_path(
            self.ctx, dataset_urn, self.config.dict(), builder.get_aspect_if_available(mce, BrowsePathsClass)
        )
        builder.set_aspect(mce, path, BrowsePathsClass)

        if has_event_stream:
            stream_config = self.topic_to_config[topic]

            event_stream_mce = create_event_stream_mce(stream_config["stream"], self.config.env)
            # systemMetadata allows DataHub to show Last Synchronized info.
            event_stream_mce.systemMetadata = mce.systemMetadata

            path = create_slash_seperated_data_path(self.ctx, event_stream_mce.proposedSnapshot.urn, self.config.dict())
            builder.set_aspect(event_stream_mce, path, BrowsePathsClass)
            for url in self.config.schema_repos:
                schema_dict = get_latest_schema_dict(url, stream_config["schema_title"])
                if schema_dict:
                    add_event_schema_metadata(dataset_urn, mce, event_stream_mce, schema_dict, stream_config)

            if not is_dry_run:
                self.ctx.graph.emit_mce(event_stream_mce)

        return mce
