"""
# This job computes user, page and denormalized history
# It doesn't depend on hive partitions being created but rather
# depends on hive data since it reads files.
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import artifact, default_args, hadoop_name_node
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

dag_id = "mediawiki_history_denormalize"
var_props = VariableProperties(f"{dag_id}_config")
snapshot = "{{data_interval_start | to_ds_month}}"

# list of tables needed for job to run.
project_namespace = ["project_namespace_map"]
raw_datasets = ["archive", "change_tag", "change_tag_def", "logging", "page", "revision", "user", "user_groups"]
raw_private_datasets = ["actor", "comment"]
datasets = project_namespace + raw_datasets + raw_private_datasets

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 3, 1)),
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_hive",
        "uses_spark",
        "requires_wmf_raw_mediawiki_actor",
        "requires_wmf_raw_mediawiki_archive",
        "requires_wmf_raw_mediawiki_change_tag",
        "requires_wmf_raw_mediawiki_change_tag_def",
        "requires_wmf_raw_mediawiki_comment",
        "requires_wmf_raw_mediawiki_logging",
        "requires_wmf_raw_mediawiki_page",
        "requires_wmf_raw_mediawiki_revision",
        "requires_wmf_raw_mediawiki_user",
        "requires_wmf_raw_mediawiki_user_groups",
        "requires_wmf_raw_project_namespace_map",
    ],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(days=4),
        },
    ),
    user_defined_filters=filters,
) as dag:
    sensors = []

    for dataset in datasets:
        if dataset in raw_datasets:
            origin_path = var_props.get("raw_origin_path", "/wmf/data/raw/mediawiki/tables")
        elif dataset in raw_private_datasets:
            origin_path = var_props.get("private_origin_path", "/wmf/data/raw/mediawiki_private/tables")
        else:  # table in project_namespace
            origin_path = var_props.get("project_namespace_path", "/wmf/data/raw/mediawiki")

        url_path = f"{hadoop_name_node}{origin_path}/{dataset}"

        # Note that we need not check if partition is present.
        # Sensor will only check that table data is present in the hdfs data path.
        sensor = URLSensor(
            task_id=f"wait_for_data_in_{dataset}",
            url=url_path + f"/snapshot={snapshot}/_SUCCESS",
            poke_interval=timedelta(hours=1).total_seconds(),
        )
        sensors.append(sensor)

    denormalize = SparkSubmitOperator(
        task_id="denormalize_history",
        executor_memory="32G",
        executor_cores=3,
        driver_memory="32G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 90,
            "spark.executor.memoryOverhead": 8192,
        },
        application=var_props.get("refinery_job_jar", artifact("refinery-job-0.2.24-shaded.jar")),
        java_class="org.wikimedia.analytics.refinery.job.mediawikihistory.MediawikiHistoryRunner",
        application_args=[
            "--snapshot",
            snapshot,
            "--mediawiki-base-path",
            var_props.get("base-path", f"{hadoop_name_node}/wmf/data/raw/mediawiki"),
            "--mediawiki-private-base-path",
            var_props.get("private-base-path", f"{hadoop_name_node}/wmf/data/raw/mediawiki_private"),
            "--output-base-path",
            var_props.get("output-base-path", f"{hadoop_name_node}/wmf/data/wmf/mediawiki"),
            "--temporary-path",
            var_props.get("tmp_path", f"{hadoop_name_node}/tmp/mediawiki/history/checkpoints"),
            "--base-num-partitions",
            64,
            "--no-stats",
        ],
    )

    sensors >> denormalize
