"""
This job reads the data from the mediawiki_history data set, rehashes it into a convenient set of partitions,
and outputs it in the form of a Bzip2 TSV dump. These dumps are meant to be public and available for download
by the community, as a resource for researchers and data scientists/analysts around the world.

More info at: https://github.com/wikimedia/analytics-refinery/blob/master/oozie/mediawiki/history/dumps/README.md
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from analytics.config.dag_config import artifact, default_args, hadoop_name_node
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "mediawiki_history_dumps"

var_props = VariableProperties(f"{dag_id}_config")
current_month = "{{ data_interval_start | to_ds_month }}"

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 8, 1)),
    schedule="@monthly",
    tags=["monthly", "from_hive", "to_hdfs", "uses_spark", "requires_wmf_mediawiki_history"],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(days=5),
        },
    ),
    user_defined_filters=filters,
) as dag:
    sensor = NamedHivePartitionSensor(
        task_id="wait_for_mediawiki_history",
        partition_names=[f"wmf.mediawiki_history/snapshot={current_month}"],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    temp_directory = var_props.get(
        "temp_directory", f"{hadoop_name_node}/wmf/tmp/analytics/mediawiki_history_dumps/{{{{ts_nodash}}}}"
    )
    output_base_path = var_props.get("output_base_path", f"{hadoop_name_node}/wmf/data/archive/mediawiki/history")
    dump = SparkSubmitOperator(
        task_id="process_mediawiki_history_dumper",
        application=var_props.get("refinery_job_jar", artifact("refinery-job-0.2.24-shaded.jar")),
        java_class="org.wikimedia.analytics.refinery.job.mediawikihistory.MediawikiHistoryDumper",
        application_args={
            "--snapshot": current_month,
            "--input-base-path": f"{hadoop_name_node}/wmf/data/wmf/mediawiki/history",
            "--temp-directory": temp_directory,
            "--temp-partitions": var_props.get("temp-partitions", "4096"),
            "--output-base-path": output_base_path,
        },
        driver_memory="16G",
        executor_memory="16G",
        executor_cores=2,
        conf={
            "spark.executor.memoryOverhead": 4096,
            "spark.dynamicAllocation.maxExecutors": 80,
            "spark.hadoop.fs.permissions.umask-mode": "022",
        },
    )

    sensor >> dump
