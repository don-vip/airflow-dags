"""
#### Compute monthly metrics on mediawiki_history snapshot
"""
from datetime import datetime, timedelta

from airflow.operators.python import BranchPythonOperator

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hdfs_temp_directory,
)
from analytics.dags.anomaly_detection.anomaly_detection_dag_factory import should_alert
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.email import HdfsEmailOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.util import sanitize_string

dag_id = "mediawiki_history_metrics_monthly"
run_id = "{{run_id}}"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2024, 3, 18),
    mw_history_metrics_application=artifact("refinery-job-0.2.34-shaded.jar"),
    mw_history_table="wmf.mediawiki_history",
    metric_table="wmf_data_ops.data_quality_metrics",  # TODO: when more DQ jos are added,
    # we should refactor this to a global config
    alerts_table="wmf_data_ops.data_quality_alerts",  # TODO: when more DQ jos are added,
    # we should refactor this to a global config
    new_snapshot="{{data_interval_start | to_ds_month}}",
    previous_snapshot="{{data_interval_start | start_of_previous_month | to_ds_month}}",
    alerts_recipients=[],
    alerts_output_path=f"{hdfs_temp_directory}/mediawiki_alerts/history/{run_id}",
    hadoop_name_node=hadoop_name_node,
    # sla and alert email
    dag_sla=timedelta(days=4),
    alerts_email=alerts_email,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["to_hive", "uses_spark", "requires_wmf_mediawiki_history"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # sense source table snapshot
    wait_for_mw_history = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)

    compute_data_quality = SparkSubmitOperator(
        task_id="Check_data_quality_of_mediawiki_history_snapshot",
        executor_memory="16G",
        executor_cores=2,
        driver_memory="16G",
        conf={"spark.dynamicAllocation.maxExecutors": 90, "spark.executor.memoryOverhead": "4G"},
        application=props.mw_history_metrics_application,
        java_class="org.wikimedia.analytics.refinery.job.dataquality.MediawikiHistoryMetrics",
        application_args={
            "--source_table": props.mw_history_table,
            "--metric_table": props.metric_table,
            "--alerts_table": props.alerts_table,
            "--alerts_output_path": sanitize_string(props.alerts_output_path),
            "--partition_map": f"snapshot:{props.new_snapshot}",
            "--previous_partition_map": f"snapshot:{props.previous_snapshot}",
            "--run_id": sanitize_string(run_id),
        },
    )

    # TODO(2024-04-09): email alerts are generated but not delivered for now.
    # To be enabled after a test period. To enable email dispatch,
    # add recipients to the alerts_recipients property.
    if props.alerts_recipients:
        # Branch operator to determine whether to send alert or not.
        has_alerts = BranchPythonOperator(
            task_id="should_alert",
            python_callable=should_alert,
            op_kwargs={
                "hadoop_name_node": props.hadoop_name_node,
                "anomaly_path": props.alerts_output_path,
            },
        )

        send_alerts = HdfsEmailOperator(
            task_id="send_email",
            to=props.alerts_recipients,
            subject="Mediawiki History data quality degradation - Airflow Analytics {{ dag.dag_id }} {{ ds }}",
            html_content=("Report content:<br/>"),
            embedded_file=props.alerts_output_path,
            hadoop_name_node=props.hadoop_name_node,
        )
        # To be enabled after a test period
        wait_for_mw_history >> compute_data_quality >> has_alerts >> send_alerts
    else:
        wait_for_mw_history >> compute_data_quality
