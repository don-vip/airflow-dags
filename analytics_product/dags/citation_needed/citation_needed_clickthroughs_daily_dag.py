"""
Get the daily counts of pageviews made by users of the experimental Citation Needed
extension (who clicked on articles to visit) and insert it into the storage table
"""

from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    product_analytics_alerts_email,

)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "citation_needed_clickthroughs_daily"

props = DagProperties(
    # dag start date
    start_date=datetime(2024, 3, 21),
    # path to query file
    cx_query_file="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/43665fb77cf016e7124bf6caf33e745626a75833/citation_needed/clickthroughs/citation_needed_clickthroughs_daily.hql",
    # sources
    source_table="wmf.pageview_actor",
    # destinations
    destination_table="wmf_product.citation_needed_clickthroughs_daily",
    # various
    coalesce_partitions="1",
    # SLA and alert email
    dag_sla=timedelta(days=1),
    alerts_email=product_analytics_alerts_email,
    # spark config (defaults fail, these settings have been successfully tested)
    driver_cores=2,
    driver_memory="2G",
    executor_cores=4,
    executor_memory="8G",
    max_executors=64,
    memory_overhead=2048,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=["daily", "uses_hql", "from_hive", "to_iceberg", "requires_wmf_pageview_actor", "future_audiences", "citation_needed_experiment"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # check if partition for the day is available
    sensor = dataset("hive_wmf_pageview_actor").get_sensor_for(dag)

    # count the searches
    compute = SparkSqlOperator(
        task_id="compute_citation_needed_clickthroughs_daily",
        sql=f"{props.cx_query_file}",
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "target_day": "{{ data_interval_start.day }}",
            "target_month": "{{ data_interval_start.month }}",
            "target_year": "{{ data_interval_start.year }}",
            "coalesce_partitions": props.coalesce_partitions,
        },
        driver_cores=props.driver_cores,
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.yarn.executor.memoryOverhead": props.memory_overhead
        }
    )

    sensor >> compute
