"""
### Refine Hourly DAG

This DAG fetches the configuration of datasets from the config store,
and generates a sensor and a Refine task for each dataset.

This DAG is currently running in the test cluster for development purposes.
Please remove this file after 2024/04, including the generated tables.
"""
import logging
from datetime import datetime, timedelta
from typing import Any, Dict, List

import requests
from airflow.decorators import task, task_group

from analytics_test.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    hadoop_name_node,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

dag_id = "refine_hourly_test"

props = DagProperties(
    start_date=datetime(2024, 2, 23, 12),
    sla=timedelta(hours=6),
    email=alerts_email,
    config_store_url="https://config-store-api-poc.toolforge.org/api/airflow/refine",
    refinery_job_jar=artifact("refinery-job-0.2.29-shaded.jar"),
)

logger = logging.getLogger(__name__)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@hourly",
    tags=["hourly", "from_hive", "to_iceberg"],
    default_args={"sla": props.sla},
    catchup=False,
) as dag:

    @task(
        do_xcom_push=True,
        templates_dict={"partition_ending": "{{data_interval_start | to_gobblin_export_path}}"},
    )
    def fetch_datasets_configurations_test(**kwargs) -> List[Dict[str, str | Any]]:
        url = props.config_store_url
        response = requests.get(url, proxies={"https": "http://webproxy.eqiad.wmnet:8080"}, timeout=10)
        logger.info(f"Fetching datasets from {url}")
        logger.info(f"Response: {response.status_code}")
        response.raise_for_status()
        configurations = response.json()["datasets"]
        logger.info(f"{len(configurations)} dataset(s) to refine: {[ds['name'] for ds in configurations]}")
        return [
            conf | {"hdfs_source_dirs": raw_partition_locations(conf, kwargs["templates_dict"]["partition_ending"])}
            for conf in configurations
        ]

    @task_group()
    def refine_dataset(configuration: Dict[str, Any]):
        @task(do_xcom_push=True)
        def prepare_import_paths_urls(configuration: Dict[str, Any]) -> List[str]:
            logger.info(f"Dataset configuration: {configuration}")
            return [f"{dir}/_IMPORTED" for dir in configuration["hdfs_source_dirs"]]

        wait_for_gobblin_exports = URLSensor(
            task_id="wait_for_gobblin_export",
            poke_interval=30,  # 30 seconds
            sla=None,  # SLA are not supported for mapped tasks
            url=prepare_import_paths_urls(configuration),
        )

        @task(
            do_xcom_push=True,
            templates_dict={"hour_to_refine": "{{ data_interval_start.in_timezone('UTC').to_iso8601_string() }}"},
        )
        def prepare_application_args(configuration: Dict[str, str | Any], **kwargs) -> List[str]:
            return [
                element
                for pair in {
                    "--hour_to_refine": kwargs["templates_dict"]["hour_to_refine"],
                    "--input_paths": ",".join(configuration["hdfs_source_dirs"]),
                    "--input_format": "json",
                    "--input_schema_base_uri": configuration["input_schema_base_uri"],
                    "--input_schema_uri": configuration["input_schema_uri"],
                    "--output_table": configuration["fully_qualified_table_name"],
                    "--transform_functions": ",".join(configuration["function_to_execute"]),
                }.items()
                for element in pair
            ]

        refine = SparkSubmitOperator(
            task_id="refine_hourly_test",
            application=props.refinery_job_jar,
            java_class="org.wikimedia.analytics.refinery.job.refine.RefineSingleApp",
            application_args=prepare_application_args(configuration),
            driver_cores=1,
            driver_memory="2G",
            executor_cores=2,
            executor_memory="4G",
            sla=None,  # SLA are not supported for mapped tasks
            conf={
                "spark.dynamicAllocation.maxExecutors": 4,
                "spark.yarn.maxAppAttempts": 1,
                "write.spark.accept-any-schema": "true",
            },
        )

        wait_for_gobblin_exports >> refine

    dataset_configurations = fetch_datasets_configurations_test()
    task_groups = refine_dataset.expand(configuration=dataset_configurations)


# Helpers


def raw_partition_locations(configuration: Dict[str, Any], gobblin_partition_ending: str) -> list[str]:
    return [f"{hadoop_name_node}{path}/{gobblin_partition_ending}" for path in configuration["hdfs_source_paths"]]
