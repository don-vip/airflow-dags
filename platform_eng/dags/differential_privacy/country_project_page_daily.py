from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator
from mergedeep import merge
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator, SparkSqlOperator
from wmf_airflow_common.partitions_builder import daily_partitions
from platform_eng.config.dag_config import (
    default_args,
    artifact
)

ds='{{data_interval_start.format("YYYY-MM-DD")}}'

props = DagProperties(
    # DAG settings
    venv=artifact('differential-privacy-0.1.0.conda.tgz'),
    # dates
    start_date=datetime(2023, 2, 18),
    year='{{data_interval_start.year}}',
    month='{{data_interval_start.month}}',
    day='{{data_interval_start.day}}',
    # source table information
    source_table='wmf.pageview_actor',
    source_granularity='@hourly',
    # static publication paths
    tmp_directory=f'hdfs:///tmp/country_project_page/{ds}',
    pub_file=f'hdfs:///wmf/data/published/datasets/country_project_page/{ds}.tsv',
    hql_path='https://gitlab.wikimedia.org/repos/security/differential-privacy/-/raw/main/differential_privacy/hql/join_titles.hql',
    # spark config
    driver_cores="2",
    driver_memory="10G",
    executor_cores="4",
    executor_memory="8G",
    max_executors="80",
    driver_maxResultSize='2G',
    driver_memoryOverhead='2G',
    executor_memoryOverhead='2G',
    sql_autoBroadcastJoinThreshold='-1',
    sql_broadcastTimeout='600',
    sql_sources_partitionOverwriteMode='dynamic',
    sql_shuffle_partitions='320',
    sql_warehouse_dir='/tmp',
    hadoop_dynamic_partition=True,
    hadoop_dynamic_partition_mode='nonstrict',
)

modified_args = merge(
    {},
    default_args,
    {
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs for normal cluster reference
        # ((8GB exec mem + 2GB exec mem overhead) * 80 instances) +
        #   ((10GB driver mem + 2GB driver mem overhead) * 2 instances) = 824GB
        'driver_cores': props.driver_cores,
        'driver_memory': props.driver_memory,
        'executor_cores': props.executor_cores,
        'executor_memory': props.executor_memory,
        'conf': {
            'spark.driver.maxResultSize': props.driver_maxResultSize,
            'spark.driver.memoryOverhead': props.driver_memoryOverhead,
            'spark.dynamicAllocation.maxExecutors': props.max_executors,
            'spark.executor.memoryOverhead': props.executor_memoryOverhead,
            'spark.sql.autoBroadcastJoinThreshold': props.sql_autoBroadcastJoinThreshold,
            'spark.sql.broadcastTimeout': props.sql_broadcastTimeout,
            'spark.sql.sources.partitionOverwriteMode': props.sql_sources_partitionOverwriteMode,
            'spark.sql.shuffle.partitions': props.sql_shuffle_partitions,
            'spark.sql.warehouse.dir': props.sql_warehouse_dir,
            'spark.hadoop.hive.exec.dynamic.partition': props.hadoop_dynamic_partition,
            'spark.hadoop.hive.exec.dynamic.partition.mode': props.hadoop_dynamic_partition_mode,
        },
    }
)

with DAG(
    dag_id='country_project_page_daily_dag',
    start_date=props.start_date,
    schedule='@daily',
    default_args=modified_args,
    max_active_runs=1,
    tags=['spark', 'data_release', 'differential_privacy']
) as dag:

    s = NamedHivePartitionSensor(
        task_id='wait_for_pageview_actor',
        partition_names=daily_partitions(table=props.source_table, granularity=props.source_granularity),
        poke_interval=timedelta(minutes=60).total_seconds(),
        timeout=timedelta(hours=10).total_seconds()
    )

    do_dp_pageview_actor = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_pageview_actor",
        virtualenv_archive=props.venv,
        use_virtualenv_spark=True,
        entry_point='lib/python3.10/site-packages/differential_privacy/country_project_page_gaussian.py',
        launcher='skein',
        application_args=[props.year, props.month, props.day],
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    join_titles = SparkSqlOperator(
        task_id="join_titles",
        sql=props.hql_path,
        query_parameters={
            'destination_directory': props.tmp_directory,
            'year': props.year,
            'month': props.month,
            'day': props.day,
            'coalesce_partitions': 1
        },
    )

    clean_up = BashOperator(
        task_id="clean_up",
        bash_command=f"hdfs dfs -mv {props.tmp_directory}/*.csv {props.pub_file} && hdfs dfs -chmod +r {props.pub_file} && hdfs dfs -rm -r {props.tmp_directory}"
    )

    s >> do_dp_pageview_actor >> join_titles >> clean_up
