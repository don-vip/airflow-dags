"""
### Geoeditors weekly DP edits
Aggregate editors_daily traditional buckets of geoeditors_monthly, but on a weekly basis using DP.
Note: This dataset does NOT contain bots actions and only considers edit actions.
"""

from datetime import datetime, timedelta
from mergedeep import merge

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from platform_eng.config.dag_config import default_args, artifact
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator, SparkSqlOperator
from wmf_airflow_common.config.dag_properties import DagProperties

current_month = '{{ data_interval_start.strftime("%Y-%m") }}'

props = DagProperties(
    # DAG settings
    venv=artifact('differential-privacy-0.1.0.conda.tgz'),
    # dates
    start_date=datetime(2023, 6, 1),
    # source table information
    source_table="wmf.editors_daily",
    source_granularity='@monthly',
    # static publication paths
    tmp_directory_weekly=f'hdfs:///tmp/geoeditors_weekly/{current_month}',
    pub_file_weekly=f'hdfs:///wmf/data/published/datasets/geoeditors_weekly/{current_month}.tsv',
    hql_path_weekly='https://gitlab.wikimedia.org/repos/security/differential-privacy/-/raw/main/differential_privacy/hql/publish_geoeditors_weekly.hql',
    tmp_directory_monthly=f'hdfs:///tmp/geoeditors_monthly/{current_month}',
    pub_file_monthly=f'hdfs:///wmf/data/published/datasets/geoeditors_monthly/{current_month}.tsv',
    hql_path_monthly='https://gitlab.wikimedia.org/repos/security/differential-privacy/-/raw/main/differential_privacy/hql/publish_geoeditors_monthly.hql',
    # spark config
    driver_cores="2",
    driver_memory="10G",
    executor_cores="4",
    executor_memory="8G",
    max_executors="80",
    driver_maxResultSize='2G',
    driver_memoryOverhead='2G',
    executor_memoryOverhead='2G',
    sql_sources_partitionOverwriteMode='dynamic',
    sql_shuffle_partitions='320',
    sql_warehouse_dir='/tmp',
    hadoop_dynamic_partition=True,
    hadoop_dynamic_partition_mode='nonstrict',
)

modified_args = merge(
    {},
    default_args,
    {
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs for normal cluster reference
        # ((8GB exec mem + 2GB exec mem overhead) * 80 instances) +
        #   ((10GB driver mem + 2GB driver mem overhead) * 2 instances) = 824GB
        'driver_cores': props.driver_cores,
        'driver_memory': props.driver_memory,
        'executor_cores': props.executor_cores,
        'executor_memory': props.executor_memory,
        'conf': {
            'spark.driver.maxResultSize': props.driver_maxResultSize,
            'spark.driver.memoryOverhead': props.driver_memoryOverhead,
            'spark.dynamicAllocation.maxExecutors': props.max_executors,
            'spark.executor.memoryOverhead': props.executor_memoryOverhead,
            'spark.sql.sources.partitionOverwriteMode': props.sql_sources_partitionOverwriteMode,
            'spark.sql.shuffle.partitions': props.sql_shuffle_partitions,
            'spark.sql.warehouse.dir': props.sql_warehouse_dir,
            'spark.hadoop.hive.exec.dynamic.partition': props.hadoop_dynamic_partition,
            'spark.hadoop.hive.exec.dynamic.partition.mode': props.hadoop_dynamic_partition_mode,
        },
    }
)

with DAG(
    dag_id="geoeditors_dag",
    start_date=props.start_date,
    schedule=props.source_granularity,
    max_active_runs=1,
    tags=["spark", "hive", "geoeditors", "differential_privacy"],
    default_args=modified_args
) as dag:
    hive_sensor = NamedHivePartitionSensor(
        task_id="wait_for_editors_daily",
        partition_names=[f'{props.source_table}/month={current_month}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    do_dp_geoeditors_weekly = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_geoeditors_weekly",
        virtualenv_archive=props.venv,
        use_virtualenv_spark=True,
        entry_point='lib/python3.10/site-packages/differential_privacy/geoeditors_weekly.py',
        launcher='skein',
        application_args=[current_month, "geoeditors_weekly"],
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    do_dp_geoeditors_monthly = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_geoeditors_monthly",
        virtualenv_archive=props.venv,
        use_virtualenv_spark=True,
        entry_point='lib/python3.10/site-packages/differential_privacy/geoeditors_monthly.py',
        launcher='skein',
        application_args=[current_month, "geoeditors_monthly"],
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    publish_weekly = SparkSqlOperator(
        task_id="publish_weekly",
        sql=props.hql_path_weekly,
        query_parameters={
            'destination_directory': props.tmp_directory_weekly,
            'month': current_month,
            'coalesce_partitions': 1
        },
    )

    publish_monthly = SparkSqlOperator(
        task_id="publish_monthly",
        sql=props.hql_path_monthly,
        query_parameters={
            'destination_directory': props.tmp_directory_monthly,
            'month': current_month,
            'coalesce_partitions': 1
        },
    )

    clean_up_weekly = BashOperator(
        task_id="clean_up_weekly",
        bash_command=f"hdfs dfs -mv {props.tmp_directory_weekly}/*.csv {props.pub_file_weekly} && hdfs dfs -chmod +r {props.pub_file_weekly} && hdfs dfs -rm -r {props.tmp_directory_weekly}"
    )

    clean_up_monthly = BashOperator(
        task_id="clean_up_monthly",
        bash_command=f"hdfs dfs -mv {props.tmp_directory_monthly}/*.csv {props.pub_file_monthly} && hdfs dfs -chmod +r {props.pub_file_monthly} && hdfs dfs -rm -r {props.tmp_directory_monthly}"
    )

    clean_up_monthly << publish_monthly << do_dp_geoeditors_monthly << hive_sensor >> do_dp_geoeditors_weekly >> publish_weekly >> clean_up_weekly
