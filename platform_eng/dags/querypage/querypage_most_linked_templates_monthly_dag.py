"""
Publishes output of most linked templates query to http fileserver

The is done monthly.

"""

from datetime import datetime, timedelta

# importing from the config of another instance...
# pros: not duplicating these settings
# cons: entangling instances
from analytics.config.dag_config import (
    create_easy_dag,
    published_directory,
)
from platform_eng.config.dag_config import (
    alerts_email,
    hadoop_name_node,
    hql_directory,
    hdfs_temp_directory,
)
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.url import URLSensor

props = DagProperties(
    # NOTE: change this in Airflow variables so as not to change the tests
    # TODO: if this list starts getting long, it will create too many airflow tasks.
    #   Instead, we should implement an ArchiveByPartition operator.  The sensor and ETL
    #   could be generic and output a temporary table partitioned by wiki.
    #   ArchiveByPartition could then be a custom spark job that writes data
    #   from each partition to a single file
    wikis_to_run=["testwiki", "frwiki"],
    # Data sources and destination.
    hive_querycache_table="wmf.querycache",
    hive_templatelinks_table="wmf_raw.mediawiki_templatelinks",
    hive_linktarget_table="wmf_raw.mediawiki_linktarget",
    wmf_raw_tables_path=f"{hadoop_name_node}/wmf/data/raw/mediawiki/tables",
    wmf_raw_private_tables_path=f"{hadoop_name_node}/wmf/data/raw/mediawiki_private/tables",

    hdfs_destination_dir=f"{published_directory}/datasets/querypage/MostTranscludedPages/",
    temporary_directory=f"{hadoop_name_node}/{hdfs_temp_directory}/querypage/MostTranscludedPages/",
    # DAG start dates.
    monthly_dag_start_date=datetime(2023, 11, 1),
    # HQL query paths
    dag_hql=f"{hql_directory}/querypage/MostTranscludedPages.hql",
    # SLAs and alerts email. (longer because templatelinks and linktarget are sqooped later)
    dag_sla=timedelta(days=10),
    alerts_email=alerts_email,
)


default_tags = [
    "from_hive",
    "to_published_datasets",
    "uses_hql",
    "monthly",
    "querypage",
    "requires_mediawiki_templatelinks_monthly",
    "requires_mediawiki_linktarget_monthly",
]

with create_easy_dag(
    dag_id="querypage_most_linked_templates_monthly",
    doc_md=__doc__,
    start_date=props.monthly_dag_start_date,
    schedule="@monthly",
    tags=default_tags,
    sla=props.dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    snapshot = "{{data_interval_start | to_ds_month}}"

    templatelinks_sensor = URLSensor(
        task_id="wait_for_templatelinks_data",
        url=f"{props.wmf_raw_tables_path}/templatelinks/snapshot={snapshot}/_PARTITIONED",
        poke_interval=timedelta(hours=1).total_seconds(),
    )
    linktarget_sensor = URLSensor(
        task_id="wait_for_linktarget_data",
        url=f"{props.wmf_raw_private_tables_path}/linktarget/snapshot={snapshot}/_PARTITIONED",
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    for wiki in props.wikis_to_run:
        # with multiple wikis running in parallel, we have to write to different directories
        # otherwise the archive tasks will find multiple source files and get confused
        individual_temp_dir = f"{props.temporary_directory}/{wiki}/"

        etl = SparkSqlOperator(
            task_id=f"compute_{wiki}",
            sql=props.dag_hql,
            query_parameters={
                "wiki": wiki,
                "source_table_templatelinks": props.hive_templatelinks_table,
                "source_table_linktarget": props.hive_linktarget_table,
                "destination_directory": individual_temp_dir,
                "year": "{{ data_interval_start.year }}",
                "month": "{{ data_interval_start.month }}",
            },
            # The default spark resources configuration is enough for this job
            # as it processes relatively small data
            # But turn off compression
            conf={
                "spark.hadoop.mapred.output.compress": "false",
            },
        )

        archive_output = HDFSArchiveOperator(
            task_id=f"archive_{wiki}",
            source_directory=individual_temp_dir,
            archive_file=props.hdfs_destination_dir + f"{wiki}.json",
            expected_filename_ending=".txt",
            check_done=True,
        )

        [templatelinks_sensor, linktarget_sensor] >> etl >> archive_output
