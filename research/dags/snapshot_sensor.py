from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from airflow.sensors.base import BaseSensorOperator
from airflow.utils.task_group import TaskGroup
from research.config.dag_config import hadoop_name_node
from wmf_airflow_common.sensors.url import URLSensor

# patience is a virtue
timeout = 60 * 60 * 24 * 30
poke_interval = 60 * 10


# ~20 day delay, as the article dag itself waits for the wikitext history snapshot
def wait_for_article_features(time_partition):
    return NamedHivePartitionSensor(
        task_id="wait_for_article_features",
        partition_names=[f"research.article_features/time_partition={time_partition}"],
        poke_interval=poke_interval,
        timeout=timeout,
    )


# ~11 days delay
def wait_for_wikidata_item_page_link_snapshot(wikidata_snapshot):
    return NamedHivePartitionSensor(
        task_id="wait_for_wikidata_item_page_link_snapshot",
        partition_names=[f"wmf.wikidata_item_page_link/snapshot={wikidata_snapshot}"],
        poke_interval=poke_interval,
        timeout=timeout,
    )


def wait_for_wikidata_entity_snapshot(wikidata_snapshot):
    return NamedHivePartitionSensor(
        task_id="wait_for_wikidata_entity_snapshot",
        partition_names=[f"wmf.wikidata_entity/snapshot={wikidata_snapshot}"],
        poke_interval=poke_interval,
        timeout=timeout,
    )


# ~2 days delay
def wait_for_mediawiki_page_history_snapshot(mediawiki_snapshot):
    return NamedHivePartitionSensor(
        task_id="wait_for_mediawiki_page_history_snapshot",
        partition_names=[f"wmf.mediawiki_page_history/snapshot={mediawiki_snapshot}"],
        poke_interval=poke_interval,
        timeout=timeout,
    )


def wait_for_mediawiki_history_snapshot(mediawiki_snapshot):
    return NamedHivePartitionSensor(
        task_id="wait_for_wmf.mediawiki_history_snapshot",
        partition_names=[f"wmf.mediawiki_history/snapshot={mediawiki_snapshot}"],
        poke_interval=poke_interval,
        timeout=timeout,
    )


# Using a URLSensor for sources with multiple partitions (e.g. snapshot=xx/wiki_db=yy)


# ~1 day (sometimes longer)
def wait_for_mediawiki_revision_snapshot(mediawiki_snapshot):
    return URLSensor(
        url=f"{hadoop_name_node}/wmf/data/raw/mediawiki/tables/revision/snapshot={mediawiki_snapshot}/_PARTITIONED",
        task_id="wait_for_mediawiki_revision_snapshot",
        poke_interval=poke_interval,
        timeout=timeout,
    )


# ~1 day (sometimes longer)
def wait_for_mediawiki_page_snapshot(mediawiki_snapshot):
    return URLSensor(
        url=f"{hadoop_name_node}/wmf/data/raw/mediawiki/tables/page/snapshot={mediawiki_snapshot}/_PARTITIONED",
        task_id="wait_for_mediawiki_page_snapshot",
        poke_interval=poke_interval,
        timeout=timeout,
    )


def wait_for_mediawiki_project_namespace_map_snapshot(mediawiki_snapshot):
    return NamedHivePartitionSensor(
        task_id="wait_for_mediawiki_project_namespace_map_snapshot",
        partition_names=[
            f"wmf_raw.mediawiki_project_namespace_map/snapshot={mediawiki_snapshot}"
        ],
        poke_interval=poke_interval,
        timeout=timeout,
    )


# ~20 day (sometimes longer)
def wait_for_mediawiki_wikitext_history_snapshot(mediawiki_snapshot):
    return URLSensor(
        url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/history/snapshot={mediawiki_snapshot}/_PARTITIONED",
        task_id="wait_for_mediawiki_wikitext_history_snapshot",
        poke_interval=poke_interval,
        timeout=timeout,
    )


# ~20 days for wikitext history + 3 days for wikidiff dag
def wait_for_wikidiff_snapshot(mediawiki_snapshot):
    return URLSensor(
        url=f"{hadoop_name_node}/wmf/data/research/wikidiff/snapshot={mediawiki_snapshot}/_PARTITIONED",
        task_id="wait_for_wikidiff_snapshot",
        poke_interval=poke_interval,
        timeout=timeout,
    )
