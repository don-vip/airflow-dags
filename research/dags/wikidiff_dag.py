"""
Computes the unified diff between a revision text and the parent revision text. The columns are the same as for the wikitext history,
for both the current and the parent revision (with a `parent_` prefix), except that instead of a row `parent_revision_text`, there
is a row `parent_revision_diff`.
"""


import getpass
import logging
import math
from dataclasses import field
from datetime import datetime
from pathlib import Path

import fsspec
from airflow import DAG
from airflow.decorators import task, task_group
from airflow.operators.empty import EmptyOperator
from pydantic.dataclasses import dataclass
from workflow_utils import util

from research.config import dag_config
from research.config.wikis import WIKIS
from research.dags.snapshot_sensor import wait_for_mediawiki_wikitext_history_snapshot
from wmf_airflow_common.hooks.spark import kwargs_for_virtualenv
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.templates.time_filters import filters


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    # Pipeline arguments
    snapshot: str = "{{ data_interval_start | to_ds_month }}"
    wikis: list[str] = field(default_factory=lambda: WIKIS)
    hive_table_dir: Path = (
        # The job in this DAG is configured to write its output
        # partitioned by wikis. So each batch job will write to the same
        # path with different a subdir for each wiki in the batch.
        Path(dag_config.hdfs_temp_directory) / "wikidiff"
    )
    # if specified, an external hive table is created for the data hive_table_dir
    external_hive_table: str | None = None
    # Batching configuration
    # Desired approximate output hdfs file size
    desired_file_mb: int = 250
    # If estimated file sizes are small, the output data is repartitioned
    repartition_threshold_threshold_mb: int = 100
    # GB of wikidata to process in one job
    gb_per_batch: int = 600
    # concurrent airflow tasks to run (be mindful of spark resources...)
    max_active_tasks: int = 1
    # Spark configuration
    # This takes up ~20% resources of the cluster as of Sep, 2023.
    # The relatively high resource usage is because this job runs multiple
    # joins involving wikitext histories. Fewer resources might
    # still get the job done but with failures and retries.
    shuffle_partitions: int = 8192
    driver_memory: str = "8G"
    executor_cores: int = 4
    executor_memory: str = "24G"
    executor_memory_overhead: str = "4G"
    max_executors: int = 98


dag_id = "wikidiff"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {
    "do_xcom_push": True,
}

username = getpass.getuser()


def determine_configuration(
    wiki_db: str, mw_snapshot: str
) -> tuple[int, int | None, float]:
    """
    For a given wiki_db and snapshot:
    - compute the number of batches so that each batch process around `gb_per_batch` of data
    - number of output partitions so that output files are around the desired output size
    """

    util.fsspec_use_new_pyarrow_api(True)
    fs = fsspec.filesystem("hdfs")
    files = fs.ls(
        f"{dag_config.hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/history/snapshot={mw_snapshot}/wiki_db={wiki_db}",
        detail=True,
    )

    gb = sum([f["size"] for f in files]) / (1024**3)
    num_batches = math.ceil(gb / props.gb_per_batch)
    desired_partitions_per_batch = math.ceil(
        (gb * 1024 / props.desired_file_mb) / num_batches
    )
    # the number of partitions of the returned diff dataframe is the configured number of
    # shuffle partitions, so the expected file size is desired_file_mb*(num_files_per_batch/num_partitions)
    # if the desired number of output files is much smaller than the  desired_partitions_per_batch is

    expected_file_mb = props.desired_file_mb * (
        desired_partitions_per_batch / props.shuffle_partitions
    )
    num_repartitioned_files = (
        desired_partitions_per_batch
        if expected_file_mb < props.repartition_threshold_threshold_mb
        else None
    )

    return (num_batches, num_repartitioned_files, gb)


@task
def create_batches(snapshot: str) -> list[list[str]]:
    jobs = []
    accumulated_configs = []
    accumulated_gb = 0

    def create_accumulated_job():
        """method to create a wikidiff job for multiple small wikis"""
        wikis = [w[0] for w in accumulated_configs]
        num_files = ",".join([str(w[1]) for w in accumulated_configs])

        wikidiff_args = [
            "run-wikis",
            "--hive-table-dir",
            str(props.hive_table_dir),
            "--snapshot",
            snapshot,
            "--wikis-list",
            ",".join(wikis),
            "--num-files-list",
            num_files,
        ]

        jobs.append(wikidiff_args)

    for wiki_db in props.wikis:
        try:
            num_batches, num_repartitioned_files, gb = determine_configuration(
                wiki_db, snapshot
            )
        except FileNotFoundError:
            logging.warning(f"wikitext history does not exist for {wiki_db}")
            continue

        # collect smaller wikis into larger jobs
        if num_batches == 1 and num_repartitioned_files:
            if accumulated_gb + gb > props.gb_per_batch:
                create_accumulated_job()
                accumulated_configs.clear()
                accumulated_gb = 0

            accumulated_gb += gb
            accumulated_configs.append((wiki_db, num_repartitioned_files))
        else:
            # multiple batches for a wiki result in multiple jobs

            for batch_no in range(num_batches):
                wikidiff_args = [
                    "run-batch",
                    "--hive-table-dir",
                    str(props.hive_table_dir),
                    "--snapshot",
                    snapshot,
                    "--wiki-db",
                    wiki_db,
                    "--num-batches",
                    num_batches,
                    "--current-batch",
                    batch_no,
                ]

                if num_repartitioned_files:
                    wikidiff_args.extend(["--num-files", num_repartitioned_files])

                jobs.append(wikidiff_args)

    # create job with remaining accumulated small wikis
    if len(accumulated_configs) > 0:
        create_accumulated_job()
    return jobs


def create_external_hive_table_queries(external_hive_table: str, hive_table_dir: Path):
    create_table = f"""
CREATE EXTERNAL TABLE IF NOT EXISTS {external_hive_table} (
  `page_id`                                       bigint        COMMENT 'id of the page',
  `page_namespace`                                int           COMMENT 'namespace of the page',
  `page_title`                                    string        COMMENT 'title of the page',
  `page_redirect_title`                           string        COMMENT 'title of the redirected-to page',
  `page_restrictions`                             array<string> COMMENT 'restrictions of the page',
  `user_id`                                       bigint        COMMENT 'id of the user that made the revision; null if anonymous, zero if old system user, and -1 when deleted or malformed XML was imported',
  `user_text`                                     string        COMMENT 'text of the user that made the revision (either username or IP)',
  `revision_id`                                   bigint        COMMENT 'id of the revision',
  `revision_parent_id`                            bigint        COMMENT 'id of the parent revision, null when this is the first revision in the chain',
  `revision_timestamp`                            string        COMMENT 'timestamp of the revision (ISO8601 format)',
  `revision_minor_edit`                           boolean       COMMENT 'whether this revision is a minor edit or not',
  `revision_comment`                              string        COMMENT 'Comment made with revision',
  `revision_text_bytes`                           bigint        COMMENT 'bytes number of the revision text',
  `revision_text_sha1`                            string        COMMENT 'sha1 hash of the revision text',
  `revision_text`                                 string        COMMENT 'text of the revision',
  `revision_content_model`                        string        COMMENT 'content model of the revision',
  `revision_content_format`                       string        COMMENT 'content format of the revision',
  `user_is_visible`                               boolean       COMMENT 'true if this revision has not had its user deleted via rev_deleted',
  `comment_is_visible`                            boolean       COMMENT 'true if this revision has not had its comment deleted via rev_deleted',
  `content_is_visible`                            boolean       COMMENT 'true if this revision has not had its text content deleted via rev_deleted',
  `parent_page_id`                                       bigint,
  `parent_page_namespace`                                int,
  `parent_page_title`                                    string,
  `parent_page_redirect_title`                           string,
  `parent_page_restrictions`                             array<string>,
  `parent_user_id`                                       bigint,
  `parent_user_text`                                     string,
  `parent_revision_id`                                   bigint,
  `parent_revision_parent_id`                            bigint,
  `parent_revision_timestamp`                            string,
  `parent_revision_minor_edit`                           boolean,
  `parent_revision_comment`                              string,
  `parent_revision_text_bytes`                           bigint,
  `parent_revision_text_sha1`                            string,
  `parent_revision_diff`                                 string,
  `parent_revision_content_model`                        string,
  `parent_revision_content_format`                       string,
  `parent_user_is_visible`                               boolean,
  `parent_comment_is_visible`                            boolean,
  `parent_content_is_visible`                            boolean,
  `parent_snapshot`                                      string,
  `parent_wiki_db`                                       string
)
COMMENT
  'Unified diff for the wikitext history, see https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/src/research_datasets/wikidiff'
PARTITIONED BY (
  `snapshot` string COMMENT 'Versioning information to keep multiple datasets (YYYY-MM for regular imports)',
  `wiki_db` string COMMENT 'The wiki_db project')
STORED AS AVRO
LOCATION
  '{dag_config.hadoop_name_node+str(hive_table_dir)}'
;
"""
    repair_table = f"Msck repair table {external_hive_table}"
    return {"create_table": create_table, "repair_table": repair_table}


with DAG(
    dag_id=dag_id,
    max_active_tasks=props.max_active_tasks,
    schedule="@monthly",
    start_date=datetime(2023, 12, 1),
    user_defined_filters=filters,
    default_args=default_args,
    tags=["research"],
    catchup=False,
) as dag:
    spark_conf = {
        # use the same hash seed to ensure deterministic hashing across batches
        "spark.executorEnv.PYTHONHASHSEED": 42,
        "spark.sql.sources.partitionOverwriteMode": "dynamic",
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.sql.adaptive.enabled": "true",
        "spark.shuffle.io.maxRetries": 10,
        "spark.task.maxFailures": 10,
        "spark.shuffle.io.retryWait": "60s",
        "spark.network.timeout": "1200s",
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }

    common_args = kwargs_for_virtualenv(
        task_id="wikidiff_batcher",
        launcher="skein",
        virtualenv_archive=props.conda_env,
        entry_point="bin/wikidiff_batch.py",
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
        packages="org.apache.spark:spark-avro_2.12:3.1.2",
        sla=None,
    )

    wait_for_wikitext_history = wait_for_mediawiki_wikitext_history_snapshot(
        props.snapshot
    )
    wikidiff_batches = create_batches(props.snapshot)

    # use a dynamic dag which creates batches based on the size of the data on hdfs
    generate_wikidiff = SparkSubmitOperator.partial(**common_args).expand(
        application_args=wikidiff_batches,
    )

    @task_group(group_id="update_external_hive")
    def update_hive():
        if props.external_hive_table:
            sql_queries = create_external_hive_table_queries(
                props.external_hive_table, props.hive_table_dir
            )
            create_table = sql_queries["create_table"]
            repair_table = sql_queries["repair_table"]

            create_external_hive_table = SparkSqlOperator(
                task_id="create_external_hive_table",
                sql=create_table,
                launcher="skein",
            )

            repair_external_hive_table = SparkSqlOperator(
                task_id="repair_external_hive_table",
                sql=repair_table,
                launcher="skein",
            )
            _ = create_external_hive_table >> repair_external_hive_table
        else:
            EmptyOperator(task_id="no_external_hive_table")

    mark_partitioned = URLTouchOperator(
        task_id="write_wikidiff_partitioned_file",
        url=dag_config.hadoop_name_node
        + str(props.hive_table_dir / f"snapshot={props.snapshot}" / "_PARTITIONED"),
    )

    _ = (
        wait_for_wikitext_history
        >> wikidiff_batches
        >> generate_wikidiff
        >> update_hive()
        >> mark_partitioned
    )
