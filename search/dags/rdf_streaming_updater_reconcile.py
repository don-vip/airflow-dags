"""
Process to collect discrepancies identified by the rdf-streaming-updater producer (flink app)
and schedule reconciliation events that are fed back to this same flink application.
This process assumes that an independent flink pipeline runs per DC and thus might
have encountered different discrepancies.
Since reconciliation events are shipped back via event-gate we do not entirely control
in which DC they might produced to. This means that reconciliation events for the flink
pipeline running in eqiad might be collected in a topic prefixed with "codfw".
In order for the flink pipeline to filter the reconciliation events that are related
to the discrepancies it found a source_tag is used (via --reconciliation-source in this spark
job).
This is also the reason unlike other jobs consuming MEP topics why this spark job is scheduled
on a per DC basis.
"""
from collections import namedtuple
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.empty import EmptyOperator

from search.config.dag_config import wdqs_spark_tools, YMDH_PARTITION, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import NamedHivePartitionSensor

dag_conf = VariableProperties('rdf_streaming_updater_reconcile_conf')

merged_default_args = dag_conf.get_merged('default_args', {
    **get_default_args(),
    'start_date': dag_conf.get_datetime('start_date', datetime(2024, 2, 20, 7, 00, 00)),
    # This job is not technically speaking dependent on past, but we increase our
    # chances of making the reconciliations happier by processing them in order
    'depends_on_past': True,
})

lapsed_actions_table = dag_conf.get('lapsed_actions_table', 'event.rdf_streaming_updater_lapsed_action')
fetch_failures_table = dag_conf.get('fetch_failures_table', 'event.rdf_streaming_updater_fetch_failure')
state_inconsistencies_table = dag_conf.get('state_inconsistencies_table',
                                           'event.rdf_streaming_updater_state_inconsistency')

wdqs_source_tag_prefix = dag_conf.get('wdqs_source_tag_prefix', 'wdqs_sideoutputs_reconcile')
wcqs_source_tag_prefix = dag_conf.get('wcqs_source_tag_prefix', 'wcqs_sideoutputs_reconcile')

wdqs_events_domain = dag_conf.get('wdqs_events_domain', 'www.wikidata.org')
wcqs_events_domain = dag_conf.get('wcqs_events_domain', 'commons.wikimedia.org')

wdqs_entity_namespaces = dag_conf.get('wdqs_entity_namespaces', '0,120,146')
wcqs_entity_namespaces = dag_conf.get('wcqs_entity_namespaces', '6')

wdqs_initial_to_namespace_map = dag_conf.get('wdqs_initial_to_namespace_map', 'Q=,P=Property,L=Lexeme')
wcqs_initial_to_namespace_map = dag_conf.get('wcqs_initial_to_namespace_map', 'M=File')

eventgate_endpoint = dag_conf.get('eventgate_endpoint', 'https://eventgate-main.discovery.wmnet:4492/v1/events')
http_routes_conf = dag_conf.get('http_routes',
                                'www.wikidata.org=https://api-ro.discovery.wmnet,'
                                'commons.wikimedia.org=https://api-ro.discovery.wmnet')

# do not re-use eventgate datacenters, these events are sent directly to kafka by the corresponding
# pipeline and do not go through eventgate
updater_datacenters = dag_conf.get("updater_datacenters", ["eqiad", "codfw"])

reconciliation_stream = dag_conf.get('reconciliation_stream', 'rdf-streaming-updater.reconcile')

SideOutputs = namedtuple('SideOutputs',
                         ['dc', 'lapsed_actions', 'fetch_failures', 'state_inconsistencies'])

sensors_sla = timedelta(hours=6)


def wait_for_side_output(table: str) -> NamedHivePartitionSensor:
    return NamedHivePartitionSensor(
        task_id='wait_for_' + table,
        mode='reschedule',
        sla=sensors_sla,
        partition_names=[
            f"{table}/datacenter={dc}/{YMDH_PARTITION}"
            for dc in updater_datacenters
        ])


def wait_for_side_output_data():
    return [
        wait_for_side_output(table=lapsed_actions_table),
        wait_for_side_output(table=fetch_failures_table),
        wait_for_side_output(table=state_inconsistencies_table),
    ]


def submit_reconciliation(name: str,
                          domain: str,
                          source_tag: str,
                          entity_namespaces: str,
                          initials_to_namespace: str,
                          http_routes: str) -> SparkSubmitOperator:
    return SparkSubmitOperator(
        task_id=name,
        application=wdqs_spark_tools,
        java_class="org.wikidata.query.rdf.updater.reconcile.UpdaterReconcile",
        max_executors=25,
        executor_cores=8,
        executor_memory="16g",
        driver_memory="4g",
        application_args=[
            "--domain", domain,
            "--reconciliation-source", source_tag,
            "--event-gate", eventgate_endpoint,
            "--late-events-partition", f"{lapsed_actions_table}/{YMDH_PARTITION}",
            "--failures-partition", f"{fetch_failures_table}/{YMDH_PARTITION}",
            "--inconsistencies-partition", f"{state_inconsistencies_table}/{YMDH_PARTITION}",
            "--stream", reconciliation_stream,
            "--entity-namespaces", entity_namespaces,
            "--initial-to-namespace", initials_to_namespace,
            "--http-routes", http_routes
        ],
    )


def build_dag(dag_id: str,
              source_tag: str,
              domain: str,
              entity_namespaces: str,
              initial_to_namespace_map: str) -> DAG:
    with DAG(
            dag_id=dag_id,
            default_args=merged_default_args,
            schedule='@hourly',
            # We want hourly runs to be scheduled one ofter the other
            max_active_runs=1,
            catchup=True
    ) as streaming_updater_reconcile:
        complete = EmptyOperator(task_id='complete')
        wait_for_data = wait_for_side_output_data()
        job = submit_reconciliation(
            name=f'reconcile',
            domain=domain,
            source_tag=f"{source_tag}@$DC$",
            entity_namespaces=entity_namespaces,
            initials_to_namespace=initial_to_namespace_map,
            http_routes=http_routes_conf
        )

        wait_for_data >> job >> complete

    return streaming_updater_reconcile


wdqs_reconcile_dag = build_dag(dag_id='wdqs_streaming_updater_reconcile_hourly',
                               source_tag=wdqs_source_tag_prefix,
                               domain=wdqs_events_domain,
                               entity_namespaces=wdqs_entity_namespaces,
                               initial_to_namespace_map=wdqs_initial_to_namespace_map)

wcqs_reconcile_dag = build_dag(dag_id='wcqs_streaming_updater_reconcile_hourly',
                               source_tag=wcqs_source_tag_prefix,
                               domain=wcqs_events_domain,
                               entity_namespaces=wcqs_entity_namespaces,
                               initial_to_namespace_map=wcqs_initial_to_namespace_map)
