import json
import os

import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_test", "dags", "refine", "refine_hourly_dag.py"]


def test_refine_hourly_dag_test_loaded(dagbag, mocker, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag_id = "refine_hourly_test"
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == 5  # The task groups are not counted as tasks.

    # Mock the dataset configurations input
    dataset_configurations_fixture = os.path.join(os.path.dirname(__file__), "dataset_configurations_fixture.json")
    with open(dataset_configurations_fixture, "r") as file:
        dataset_configurations_fixture_json = json.loads(file.read())

    mock_config_store_call(dataset_configurations_fixture_json, mocker)

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(hours=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test the task in charge of getting the configuration
    fetch_datasets_configurations_test_ti = create_and_run_task("fetch_datasets_configurations_test", dagrun)
    result = fetch_datasets_configurations_test_ti.xcom_pull(
        task_ids=fetch_datasets_configurations_test_ti.task.task_id
    )
    assert len(result) == 2
    assert result[0] == dataset_configurations_fixture_json["datasets"][0] | {
        "hdfs_source_dirs": [
            "hdfs://analytics-test-hadoop/wmf/data/raw/eventlogging_legacy/eventlogging_NavigationTiming"
            "/year=2024/month=02/day=23/hour=12"
        ]
    }

    # Test the task in charge of preparing the sensor
    prepare_import_paths_urls_ti = create_and_run_task("refine_dataset.prepare_import_paths_urls", dagrun, map_index=0)
    assert prepare_import_paths_urls_ti.xcom_pull(
        task_ids=prepare_import_paths_urls_ti.task.task_id, map_indexes=0
    ) == [
        "hdfs://analytics-test-hadoop/wmf/data/raw/eventlogging_legacy/eventlogging_NavigationTiming/"
        "year=2024/month=02/day=23/hour=12/_IMPORTED"
    ]

    # Test the task in charging of preparing the parameters for the spark job
    prepare_application_args_ti = create_and_run_task("refine_dataset.prepare_application_args", dagrun, map_index=0)
    assert prepare_application_args_ti.xcom_pull(task_ids=prepare_application_args_ti.task.task_id, map_indexes=0) == [
        "--hour_to_refine",
        "2024-02-23T12:00:00Z",
        "--input_paths",
        "hdfs://analytics-test-hadoop/wmf/data/raw/eventlogging_legacy/eventlogging_NavigationTiming/"
        "year=2024/month=02/day=23/hour=12",
        "--input_format",
        "json",
        "--input_schema_base_uri",
        "https://schema.discovery.wmnet/repositories/secondary/jsonschema/",
        "--input_schema_uri",
        "analytics/legacy/navigationtiming/1.6.0",
        "--output_table",
        "aqu.navigationtiming_iceberg",
        "--transform_functions",
        "org.wikimedia.analytics.refinery.job.refine.filter_allowed_domains,org.wikimedia.analytics.refinery.job.refine.remove_canary_events,org.wikimedia.analytics.refinery.job.refine.deduplicate,org.wikimedia.analytics.refinery.job.refine.geocode_ip,org.wikimedia.analytics.refinery.job.refine.parse_user_agent,org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain,org.wikimedia.analytics.refinery.job.refine.add_normalized_host,org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes,org.wikimedia.analytics.refinery.job.refine.parseTimestampFields,org.wikimedia.analytics.refinery.job.refine.convertToDestinationSchema",  # noqa: E501
    ]

    # Test the task in charge of actually refining the dataset
    refine_hourly_test = dag.get_task("refine_dataset.refine_hourly_test")
    refine_hourly_test_ti = TaskInstance(refine_hourly_test, run_id=dagrun.run_id, map_index=0)
    refine_hourly_test_ti.dag_run = dagrun
    context = refine_hourly_test_ti.get_template_context()
    refine_hourly_test_ti.render_templates(context=context)
    skein_hook = refine_hourly_test._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id="refine_dataset.refine_hourly_test", **kwargs)


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


def mock_config_store_call(dataset_configurations_fixture_json, mocker):
    mock_get = mocker.patch("analytics_test.dags.refine.refine_hourly_dag.requests.get")
    mock_response = mock_get.return_value
    mock_response.status_code = 200
    mock_response.json.return_value = dataset_configurations_fixture_json
