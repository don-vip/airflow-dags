import pytest


@pytest.fixture
def dag_path():
    return ["research", "dags", "revert_risk_multilingual_dataset_dag.py"]


def test_revert_risk_multlingual_dataset_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="revert_risk_multilingual_dataset")
    assert dag is not None
    assert len(dag.tasks) == 7
