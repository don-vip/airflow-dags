import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["research", "dags", "risk_observatory_dag.py"]


def test_risk_observatory_pipeline_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="risk_observatory")
    assert dag is not None
    assert len(dag.tasks) == 6
