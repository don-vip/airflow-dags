import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["research", "dags", "top_pages.py"]


def test_top_pages_pipeline_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="top_pages")
    assert dag is not None
    assert len(dag.tasks) == 5
