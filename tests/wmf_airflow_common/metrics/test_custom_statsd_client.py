# Override of statsd client to generate gauges instead of timing metrics.

import random
import socket
from unittest import mock

# from statsd import StatsClient
from wmf_airflow_common.metrics.custom_statsd_client import CustomStatsClient

ADDR = (socket.gethostbyname("localhost"), 8125)
UNIX_SOCKET = "tmp.socket"


# proto specific methods to get the socket method to send data
send_method = {
    "udp": lambda x: x.sendto,
    "tcp": lambda x: x.sendall,
    "unix": lambda x: x.sendall,
}


# proto specific methods to create the expected value
make_val = {
    "udp": lambda x, addr: mock.call(str.encode(x), addr),
    "tcp": lambda x, addr: mock.call(str.encode(x + "\n")),
    "unix": lambda x, addr: mock.call(str.encode(x + "\n")),
}


def eq_(a, b, msg=None):
    assert a == b, msg


def _udp_client(prefix=None, addr=None, port=None, ipv6=False):
    if not addr:
        addr = ADDR[0]
    if not port:
        port = ADDR[1]
    sc = CustomStatsClient(host=addr, port=port, prefix=prefix, ipv6=ipv6)
    print(type(sc))
    sc._sock = mock.Mock()
    return sc


def _sock_check(sock, count, proto, val=None, addr=None):
    send = send_method[proto](sock)
    eq_(send.call_count, count)
    if not addr:
        addr = ADDR
    if val is not None:
        eq_(
            send.call_args,
            make_val[proto](val, addr),
        )


def _test_timing(cl, proto):
    cl.timing("foo", 100)
    # original: _sock_check(cl._sock, 1, proto, "foo:100.000000|ms")
    _sock_check(cl._sock, 1, proto, "foo:100.000000|g")

    cl.timing("foo", 350)
    # original: _sock_check(cl._sock, 2, proto, "foo:350.000000|ms")
    _sock_check(cl._sock, 2, proto, "foo:350.000000|g")

    cl.timing("foo", 100, rate=0.5)
    # original: _sock_check(cl._sock, 3, proto, "foo:100.000000|ms|@0.5")
    _sock_check(cl._sock, 3, proto, "foo:100.000000|g|@0.5")


@mock.patch.object(random, "random", lambda: -1)
def test_timing_udp():
    """StatsClient.timing works."""
    cl = _udp_client()
    _test_timing(cl, "udp")
