from pendulum import datetime

from wmf_airflow_common.sensors.helper import (
    daily_execution_dates_of_month,
    get_daily_execution_dates_fn,
)


def test_daily_execution_dates_of_month():
    execution_date = datetime(2023, 4, 1)
    execution_dates = daily_execution_dates_of_month(execution_date)
    assert len(execution_dates) == 30  # April has 30 days
    assert execution_dates[0].strftime("%Y-%m-%d-%H:%M") == "2023-04-01-00:00"  # First task run to look for
    assert execution_dates[-1].strftime("%Y-%m-%d-%H:%M") == "2023-04-30-00:00"  # And the last


def test_get_daily_execution_dates_fn():
    exec_date = datetime(2024, 4, 1)

    # test retrieving a week's worth of execution dates
    test_num_days = 7
    daily_exec_dates_fn = get_daily_execution_dates_fn(num_days=test_num_days)
    exec_dates = daily_exec_dates_fn(exec_date, None)

    assert exec_dates is not None
    assert len(exec_dates) == test_num_days
    assert exec_dates[0].strftime("%Y-%m-%d-%H:%M") == "2024-04-01-00:00"  # First task run to look for
    assert exec_dates[-1].strftime("%Y-%m-%d-%H:%M") == "2024-04-07-00:00"  # And the last

    # test retrieving the default value (one day's worth)
    daily_exec_dates_fn = get_daily_execution_dates_fn()
    exec_dates = daily_exec_dates_fn(exec_date, None)

    assert exec_dates is not None
    assert len(exec_dates) == 1
    assert exec_dates[0].strftime("%Y-%m-%d-%H:%M") == "2024-04-01-00:00"  # First task run to look for
