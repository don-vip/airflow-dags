<!---
Thank you for your merge request! 🚀
-->

### Contributor checklist

<!--
Please replace the empty checkboxes [] below with checked ones [x] accordingly.
Please also replace PATH_TO_JOB_TESTS with an appropriate path within wmde/analytics/hql.
-->

- [] I have written tests for this DAG that will be merged into [data-engineering/airflow-dags/tests/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/tests/wmde)
- [] I have locally ran the above tests as outlined in the [tests section of the Airflow DAGs project readme](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags#running-tests)
- [] I have tested the jobs for this DAG in my local database using the process defined in [wmde/analytics/hql/airflow-jobs/PATH_TO_JOB_TESTS](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow-jobs/PATH_TO_JOB_TESTS)
- [] All tables that the jobs for this DAG interact with exist in HDFS (or I will contact WMF Data Engineering on Slack)

### Description

<!--
Please describe briefly what your merge request proposes to change. Especially if you have more than one commit, it is helpful to give a summary of what your contribution is trying to solve.
-->

### Test outputs

<!--
Please describe the outputs of the tests that were ran on your local database. Ideally include the sanitized outputs of queries so that the results can be compared against expected outputs.
 -->

### Related task

<!---
Please link to the appropriate Phabricator task by replacing TASK_NUMBER below.
-->

- [TASK_NUMBER](https://phabricator.wikimedia.org/TASK_NUMBER)
