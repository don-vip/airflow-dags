"""
Collects monthly distinct aggregates for user agents and ips using the Wikidata REST API.
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import alerts_email, create_easy_dag, dataset
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    start_date=datetime(2024, 2, 1),
    hql_uri=(
        "https://gitlab.wikimedia.org/repos/wmde/analytics"
        "/-/raw/main/hql/airflow-jobs/wd_rest_api_metrics/wd_rest_api_metrics_monthly.hql"
    ),
    webrequest_table="wmf.webrequest",
    wd_rest_api_metrics_monthly_table="wmde.wd_rest_api_metrics_monthly",
    sla=timedelta(hours=6),
    alerts_email=alerts_email,
)

with create_easy_dag(
    dag_id="wd_rest_api_metrics_monthly",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    compute = SparkSqlOperator(
        task_id="compute_metrics",
        sql=props.hql_uri,
        query_parameters={
            "source_table": props.webrequest_table,
            "destination_table": props.wd_rest_api_metrics_monthly_table,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 16,
            "spark.yarn.executor.memoryOverhead": 2048,
        },
    )

    sensor >> compute
