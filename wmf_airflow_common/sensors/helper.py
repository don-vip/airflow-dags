from datetime import datetime, timedelta
from typing import Any, Callable


def daily_execution_dates_of_month(execution_date=None):
    """
    Return the execution dates of the daily DAG for the month of the given execution date.
    Used in the context of a TaskSensor to wait for the daily DAG to complete for the month of the given execution date.

    :param context: The context for the task_instance of interest.
    :return: A list of execution dates of the daily DAG for the month of the given execution date.
    """
    if execution_date is None:
        return []
    month_start = execution_date.replace(day=1)
    month_end = month_start + timedelta(days=32)
    month_end = month_end.replace(day=1) - timedelta(days=1)
    return [month_start + timedelta(days=d) for d in range(0, (month_end - month_start).days + 1)]


def get_daily_execution_dates_fn(num_days=1) -> Callable[[datetime, Any], list[datetime]]:
    """Return a function that generates the desired number of execution dates
    when provided with a starting execution date and the Airflow Context object.

    The list of generated execution dates starts with the execution date
    and goes forward in time.

    :param num_days: Number of days in the list the returned function will generate
    """

    def _get_dates(exec_date: datetime | None = None, context=None) -> list[datetime]:
        if exec_date is None:
            return []
        return [
            (exec_date + timedelta(days=i)).replace(hour=0, minute=0, second=0, microsecond=0) for i in range(num_days)
        ]

    return _get_dates
